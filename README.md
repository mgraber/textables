textables: Customized LaTeX tables in R
================
Created by Thibaut Lamadon and Bradley Setzler, University of Chicago

Overview
--------

This package produces highly-customized LaTeX tables in R. The broad organization of functions is as follows:

-   `tt_numeric_*`: Functions to add numbers to tables;
-   `tt_text_*`: Functions to add text to tables;
-   `tt_rule_*`: Functions to add rules to tables;
-   `tt_spacer_*`: Functions to add spacing to tables;
-   `tt_tabularize` and `tt_save`: Functions to output the table to LaTeX.

It supports building a table in blocks, in the spirit of ggplot2, using the `+` and `%:%` operators for concatenation.

Here is an example of the type of table that this package can easily construct:

![Example Table](inst/example.png)

The package can be installed with the command `devtools::install_github("setzler/textables")`.

Details
-------

### Numeric Columns and Rows: `tt_numeric_*`

The functions for constructing numeric columns and rows are as follows:

-   `tt_numeric_column`: a numeric column; and,
-   `tt_numeric_row`: a numeric row, with argument `cspan` that allows the numbers to span multiple columns.

Numeric formatting options include:

-   `dec`: control decimal places, for example, `dec=3` displays 3 decimal places;
-   `se`: surround numbers with parenthesis with `se=TRUE`;
-   `percentage`: add a percentage sign to each number with `percentage=TRUE`;
-   `pvalues`: use p-values to add stars to indicating significance, for example, `pvalues=c(0.005,0.05)` would add 3 stars and 2 stars, respectively.

### Text Columns and Rows: `tt_text_*`

The functions for constructing text columns and rows are:

-   `tt_text_column`: a text column.
-   `tt_text_row`: a text row, with argument `cspan` that allows the text to span multiple columns.

### Rules: `tt_rule_*`

-   `tt_rule_top`: add a top-rule;
-   `tt_rule_mid`: add a mid-rule;
-   `tt_rule_bottom`: add a bottom-rule;
-   `tt_midrule_partial`: add a partial mid-rule (user must supply begin and end points in a list of vectors, e.g., `tt_midrule_partial = list(c(1,2),c(3,4))`).

### Spacers: `tt_spacer_*`

-   `tt_spacer_row`: adds vertical space between rows, for example, `tt_spacer_rows(4.5)` adds 4.5pt of vertical space before the next row begins.

### Concatenation: `+` and `%:%` operators

-   `+`: binds rows together vertically; and,
-   `%:%`: binds columns together horizontally.

The output from `tt_numeric_column` and `tt_text_column` can be combined into a single table with `+`, while the output from `tt_numeric_row` and `tt_text_row` could be combined into a single table with `%:%`.

### Finishing and Exporting: `tt_tabularize` and `tt_save`

-   `tt_tabularize`: Converts a tt object into a tabular by collapsing into TeX code with begin/end tabular commands;
-   `tt_save`: save as a .tex file. The `stand_alone=T` option makes it a document that can be compiled directly by LaTeX. The `tabularize_output=T` option runs `tabularize` on the tt object before exporting.

Example
-------

This example demonstrates the construction of the example table seen at the beginning of this document.

### 1. Install and load the package

``` r
devtools::install_github("setzler/textables")
```

``` r
library(textables)
```

### 2. Construct example regression output

``` r
library(data.table)

dd <- data.table(
    sample = c("Full Sample", "Full Sample", "Subsample", "Subsample"), 
    controls = c("No", "Yes", "No", "Yes"), 
    coef = c(1.17, 1.59, 1.105, 1.69),
    SEs = c(.6, .481, .789, .8), 
    pvals = c(.051, .001, .16, .091), 
    N = c(1234567, 1234567, 891011, 891011)
  )

print(dd)
```

    ##         sample controls  coef   SEs pvals       N
    ## 1: Full Sample       No 1.170 0.600 0.051 1234567
    ## 2: Full Sample      Yes 1.590 0.481 0.001 1234567
    ## 3:   Subsample       No 1.105 0.789 0.160  891011
    ## 4:   Subsample      Yes 1.690 0.800 0.091  891011

### 3. Text rows, rules, and spacers

We start by creating a text row indicating whether or not controls are included:

``` r
tt <- with(dd, tt_text_row(controls)) 

print(tt)
```

    ## % created using textables on Wed Feb 20 00:28:50 2019
    ## \begin{tabular}{r}
    ## No & Yes & No & Yes \\
    ## \end{tabular}

Note that, when printed to the console, a tt object will automatically include the begin/end tabular lines as a convenience in case the user wishes to test out the appearance of the table without relying on textable's exporting methods described below.

We append to this a row label, add 1pt of space, and follow it with a midrule:

``` r
tt <- tt_text_row("Controls") %:% tt + tt_spacer_row(1) + tt_rule_mid()

print(tt)
```

    ## % created using textables on Wed Feb 20 00:28:50 2019
    ## \begin{tabular}{rr}
    ## Controls & No & Yes & No & Yes \\[1pt]
    ## \midrule 
    ## \end{tabular}

We add a row above with labels that span multiple columns with matching partial midrules:

``` r
tt = tt_text_row(" ") %:% with(dd, tt_text_row(unique(sample), cspan=c(2, 2))) + 
  tt_rule_mid_partial(list(c(2,3),c(4,5)))  + tt

print(tt)
```

    ## % created using textables on Wed Feb 20 00:28:50 2019
    ## \begin{tabular}{rr}
    ##   & \multicolumn{2}{c}{Full Sample} & \multicolumn{2}{c}{Subsample} \\
    ##  \cmidrule(lr){2-3} \cmidrule(lr){4-5} 
    ## Controls & No & Yes & No & Yes \\[1pt]
    ## \midrule 
    ## \end{tabular}

### 4. Constructing numeric rows

We create a row of coefficient estimates with stars indicating significance and rounding to the third decimal place:

``` r
tt <- tt +  tt_text_row("Coefficient ($\\tilde{\\beta}_\\nu$)") %:% 
      with(dd, tt_numeric_row(coef, pvalues=pvals, dec=2))

print(tt)
```

    ## % created using textables on Wed Feb 20 00:28:50 2019
    ## \begin{tabular}{rr}
    ##   & \multicolumn{2}{c}{Full Sample} & \multicolumn{2}{c}{Subsample} \\
    ##  \cmidrule(lr){2-3} \cmidrule(lr){4-5} 
    ## Controls & No & Yes & No & Yes \\[1pt]
    ## \midrule 
    ## Coefficient ($\tilde{\beta}_\nu$) & 1.17* & 1.59*** & 1.10 & 1.69* \\
    ## \end{tabular}

We add to this a row of standard errors:

``` r
tt <- tt +  tt_text_row("Std. Error") %:% 
      with(dd, tt_numeric_row(SEs, se=T, dec=2))
```

Finally, we add a row of sample size integers (note the automatic commas, which can be disabled with `big.mark=""`). We separate the sample size with a midrule:

``` r
tt <- tt + tt_rule_mid() + tt_text_row("Sample Size") %:% 
      with(dd, tt_numeric_row(unique(N), cspan=c(2,2), dec=0))

print(tt)
```

    ## % created using textables on Wed Feb 20 00:28:50 2019
    ## \begin{tabular}{rr}
    ##   & \multicolumn{2}{c}{Full Sample} & \multicolumn{2}{c}{Subsample} \\
    ##  \cmidrule(lr){2-3} \cmidrule(lr){4-5} 
    ## Controls & No & Yes & No & Yes \\[1pt]
    ## \midrule 
    ## Coefficient ($\tilde{\beta}_\nu$) & 1.17* & 1.59*** & 1.10 & 1.69* \\
    ## Std. Error & (0.60) & (0.48) & (0.79) & (0.80) \\
    ## \midrule 
    ## Sample Size & \multicolumn{2}{c}{1,234,567} & \multicolumn{2}{c}{891,011} \\
    ## \end{tabular}

### 5. Finishing and saving

We convert the tt object into a LaTeX tabular using `tt_tabularize`. We can make the tabular prettier with top and bottom rules with the `pretty_rules=T` option:

``` r
tab <- tt_tabularize(tt, header=c("l",rep("r",4)), pretty_rules=T)

print(tab)
```

    ##  [1] "% created using textables on Wed Feb 20 00:28:50 2019"                           
    ##  [2] "\\begin{tabular}{lrrrr}"                                                         
    ##  [3] "\\toprule "                                                                      
    ##  [4] "\\midrule "                                                                      
    ##  [5] "  & \\multicolumn{2}{c}{Full Sample} & \\multicolumn{2}{c}{Subsample} \\\\"      
    ##  [6] " \\cmidrule(lr){2-3} \\cmidrule(lr){4-5} "                                       
    ##  [7] "Controls & No & Yes & No & Yes \\\\[1pt]"                                        
    ##  [8] "\\midrule "                                                                      
    ##  [9] "Coefficient ($\\tilde{\\beta}_\\nu$) & 1.17* & 1.59*** & 1.10 & 1.69* \\\\"      
    ## [10] "Std. Error & (0.60) & (0.48) & (0.79) & (0.80) \\\\"                             
    ## [11] "\\midrule "                                                                      
    ## [12] "Sample Size & \\multicolumn{2}{c}{1,234,567} & \\multicolumn{2}{c}{891,011} \\\\"
    ## [13] "\\midrule "                                                                      
    ## [14] "\\bottomrule "                                                                   
    ## [15] "\\end{tabular}"

We can use `tt_save` to save the tabular to a .tex file. If we want to be able to compile the .tex file directly, we can use the `stand_alone=TRUE` option, then use `texi2pdf` command from the tools package to compile to PDF:

``` r
setwd("inst")
tt_save(tab,filename='example.tex',stand_alone = TRUE)
tools::texi2pdf("example.tex", clean = TRUE)
```

The resulting table is displayed at the beginning of this document.
